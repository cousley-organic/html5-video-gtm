# HTML5-VIDEO-GTM

Module to track HTML5 video element events using Google Tag Manager

## Installation

Use the package manager [npm](https://www.npmjs.com/) to install `HTML5-video-gtm`.

```bash
npm i git+https://cousley-organic@bitbucket.org/cousley-organic/html5-video-gtm.git --save
```

or...

```bash
npm i ssh://bitbucket.org/cousley-organic/html5-video-gtm.git --save
```

## Usage

Include module's script in your Gulp file's script processor. Include your google analytic's tag JSON with it.

```javascript
gulp.task('scripts', [], function () {
    return gulp.src([
        './app/js/tracking/tracking.meta.js',
        './node_modules/html5-video-gtm/js/main.js',
    ])
})
```

Set up each ```<VIDEO>``` element with the following classes and attributes:

* *class="**myVideo**"*
* *data-duration="__{{ video duration in seconds }}__"*
* *data-percent=""*
* *data-track-video="__{{ video tracking name from tag JSON }}__"*
* *playsinline* <-- for compatibility with iOS 

```html
<video
  class="myVideo"
  data-duration="65" 
  data-percent="" 
  data-track-video="masthead-video"
  playsinline
 >
  <source src="" type="video/mp4">
</video>
```


## Contributing
Contributions, suggestions, and forks are welcome.

Fork Freely!

For changes to this repo, please contact the author.

Name:|Clayton G. Ousley
-----|-----
Phone: | 734-726-0984
Email: | cousley@organic.com
Slack: | Clayton Ousley
 

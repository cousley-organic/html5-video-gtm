window.addEventListener('load', function () {

	let videos = document.querySelectorAll('.myVideo');

	Array.from(videos).forEach(function (video) {

		video.addEventListener("timeupdate", videoTimeUpdate, false);

		// NEEDS COMPLETION
		video.addEventListener("ended", videoEnd, false);
		video.addEventListener("play", videoPlay, false);
		video.addEventListener("pause", videoPause, false);


		function videoTimeUpdate(e) {
			const duration = e.target.getAttribute('data-duration');
			const duration_25 = duration * 0.25;
			const duration_50 = duration * 0.50;
			const duration_75 = duration * 0.75;
			const duration_100 = duration;

			let tagPush;

			//grab the current time from the video.
			var curTime = e.target.currentTime;
			var lastPercent = e.target.getAttribute('data-percent');
			var video_name = e.target.getAttribute('data-track-video');

			if (curTime >= duration_100 && lastPercent === '75') {
				e.target.setAttribute('data-percent', '100');
				tagPush = 1;
			}
			else if (curTime >= duration_75 && lastPercent === '50') {
				e.target.setAttribute('data-percent', '75');
				tagPush = 0.75;
			}
			else if (curTime >= duration_50 && lastPercent === '25') {
				e.target.setAttribute('data-percent', '50');
				tagPush = 0.5;
			}
			else if (curTime >= duration_25 && lastPercent === '0') {
				e.target.setAttribute('data-percent', '25');
				tagPush = 0.25;
			}
			else if (curTime < duration_25 && lastPercent === '') {
				e.target.setAttribute('data-percent', '0');
				tagPush = 0;
			}
			for (let i = 0; i < timestamps[video_name].length; i++) {
				if (
					tagPush === timestamps[video_name][i].trigger
				) {
					// console.log('match')
					sendTrackingdata(timestamps[video_name][i]);
				}
			}
		}

		// NEEDS COMPLETION
		function videoEnd() {
			// console.log('videoEnd');
		}
		function videoPlay() {
			// console.log('videoPlay');
		}
		function videoPause() {
			// console.log('videoPause');
		}


		function sendTrackingdata(obj) {
			// console.log({obj});
			// console.log('obj.event is: ' + obj.google.event + '.');
			// console.log('obj.cat is: ' + obj.google.cat + '.');
			// console.log('obj.action is: ' + obj.google.action + '.');
			// console.log('obj.label is: ' + obj.google.label + '.');


			if (typeof obj != "undefined") {
				/////////////////////////////////////////////////////////////
				// Google Video tracking
				/////////////////////////////////////////////////////////////
				if (obj.google) {
					var feed = {
						event: obj.google.event,
						lineOfBusiness: obj.google.cat,
						userActionTaken: obj.google.action,
						subproduct: obj.google.label,
						buttonLocation: obj.google.location,
					};
					window.dataLayer.push(feed);
				}

			}
		}
	});

});

